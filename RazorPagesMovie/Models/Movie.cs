﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RazorPagesMovie.Models
{
    public class Movie
    {
        // Properties
        public int ID { get; set; }

        [StringLength(60, MinimumLength = 3)] // Max of 60 characters, minimum of 3, and required field.
        [Required]
        public string Title { get; set; }
        
        [Display(Name = "Release Date")] // Specifies how to display this column in the web page.
        [DataType(DataType.Date)] // Specifies that ReleaseDate will be a date only without the time.
        public DateTime ReleaseDate { get; set; }

        [RegularExpression(@"^[A-Z]+[a-zA-Z\s]*$")]
        [StringLength(30)]
        [Required]
        public string Genre { get; set; }

        [Range(1, 100)]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")] // Correctly maps price to currency in the DB.
        public decimal Price { get; set; }

        [RegularExpression(@"^[A-Z]+[a-zA-Z0-9""'\s-]*$")]
        [StringLength(5)]
        [Required]
        public string Rating { get; set; }

        public Movie()
        {
        }
    }
}
